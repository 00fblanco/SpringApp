package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootH2 {

    public static void main(String args[]){
        SpringApplication.run(SpringBootH2.class, args);
    }
}
